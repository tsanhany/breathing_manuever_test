import 'package:bloc_test/bloc_test.dart';
import 'package:breathing_manuever_test/bloc/bloc/breath_bloc.dart';
import 'package:breathing_manuever_test/data/models/breath_phase.dart';
import 'package:breathing_manuever_test/utils/constants.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  setUp() {}

  group("Testing the breath bloc simulation", () {
    blocTest<BreathBloc, BreathState>(
      'emits [BreathInProgress(phase)] when BreathValueReceivedEvent is added.',
      build: () => BreathBloc(),
      act: (bloc) {
        return bloc
          ..add(BreathValueReceivedEvent(1))
          ..add(BreathValueReceivedEvent(4))
          ..add(BreathValueReceivedEvent(10))
          ..add(BreathValueReceivedEvent(95))
          ..add(BreathValueReceivedEvent(100));
      },
      expect: () => [
        BreathInProgress(BreathPhase(
            id: Constants.startPhaseId,
            text: Constants.startPhaseText,
            percentage: 1)),
        BreathInProgress(BreathPhase(
            id: Constants.startPhaseId,
            text: Constants.startPhaseText,
            percentage: 4)),
        BreathInProgress(BreathPhase(
            id: Constants.secondPhaseId,
            text: Constants.secondPhaseText,
            percentage: 10)),
        BreathInProgress(BreathPhase(
            id: Constants.thirdPhaseId,
            text: Constants.finalPhaseText,
            percentage: 95)),
        BreathInProgress(BreathPhase(
            id: Constants.finalPhaseId,
            text: Constants.finalPhaseText,
            percentage: 100)),
      ],
    );
  });
}
