import 'package:flutter/material.dart';

class LumenColors {
  static const Color purple = Color.fromARGB(255,83,54,118);
  static const Color blue = Color.fromARGB(255,60,133,140);
  static const Color lightGrey = Color.fromARGB(150,192, 196, 209);

}
