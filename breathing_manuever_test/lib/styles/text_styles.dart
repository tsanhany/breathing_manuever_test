import 'package:breathing_manuever_test/styles/lumen_colors.dart';
import 'package:flutter/material.dart';

class TextStyles {
  static TextStyle blueBoldText = const TextStyle(
    color: LumenColors.blue,
    fontFamily: "Urbanist",
    fontWeight: FontWeight.w600,
    fontSize: 34
  );

  static TextStyle purpleBoldText = const TextStyle(
    color: LumenColors.purple,
    fontFamily: "Urbanist",
    fontSize: 70
  );
}
