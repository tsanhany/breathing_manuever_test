import 'package:breathing_manuever_test/bloc/bloc/breath_bloc.dart';
import 'package:breathing_manuever_test/styles/lumen_colors.dart';
import 'package:breathing_manuever_test/utils/constants.dart';
import 'package:breathing_manuever_test/widgets/animated_circle.dart';
import 'package:breathing_manuever_test/widgets/animated_text.dart';
import 'package:breathing_manuever_test/widgets/dashed_rings.dart';
import 'package:breathing_manuever_test/widgets/next_btn.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BreathScreen extends StatefulWidget {
  const BreathScreen({Key? key}) : super(key: key);

  @override
  State<BreathScreen> createState() => _BreathScreenState();
}

class _BreathScreenState extends State<BreathScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(children: [
        Positioned(
            top: 70,
            left: 25,
            child: BlocBuilder<BreathBloc, BreathState>(
              buildWhen: (previous, current) =>
                  previous.phase.id != current.phase.id,
              builder: (context, state) {
                return AnimatedText(
                  text: state.phase.text,
                );
              },
            )),
         Center(
              child: Stack(children: [
                const DashedCircles(),
                Center(
                  child: BlocBuilder<BreathBloc, BreathState>(
                    builder: (context, state) {
                      return _animatedCircle(state);
                    },
                  ),
                )
              ]),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: _buttonSection(),
              ),
      ]),
    );
  }

  Container _buttonSection() {
    return Container(
              margin: const EdgeInsets.symmetric(vertical: 50),
              child: BlocBuilder<BreathBloc, BreathState>(
                buildWhen: (previous, current) => previous.phase.id != current.phase.id,
                builder: (context, state) {
                  double opacity;
                  Widget button; 
                  switch (state.phase.id) {
                    case Constants.initialPhaseId: 
                      opacity = 1;
                      button = NextBtn(
                        text: Constants.phase1Btn_start,
                        onPressed: () => BlocProvider.of<BreathBloc>(context).add(StartInhalingEvent())
                      );
                      break;
                    case Constants.finalPhaseId:
                      opacity = 1;
                      button = NextBtn(
                        text: Constants.phase4Btn_replay,
                        onPressed: () => BlocProvider.of<BreathBloc>(context).add(StartInhalingEvent())
                      );
                      break;
                    default:
                      opacity = 0;
                      button = const SizedBox();
                  }
                  return AnimatedOpacity(
                                curve: Curves.linear,
                                duration: const Duration(seconds: 1),
                                opacity: opacity,
                                child: button);
                },
              ),
              );
  }

  AnimatedContainer _animatedCircle(BreathState state) {
    double percentage = state.phase.percentage.toDouble();
    return AnimatedContainer(
        duration: const Duration(milliseconds: 400),
        width: percentage,
        child: const AnimatedCircle());
  }

}
