import 'package:breathing_manuever_test/styles/lumen_colors.dart';
import 'package:flutter/material.dart';

class AnimatedCircle extends StatelessWidget {
  const AnimatedCircle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: BreathPainter(),
    );
  }
}

class BreathPainter extends CustomPainter {
  late Paint _breathPaint;

  BreathPainter();

  @override
  void paint(Canvas canvas, Size size) {
   // double radius = (size.width / 2) * percentage - 30;
    double radius = (size.width / 100) * 180 - 30;
    _breathPaint = Paint()
      ..shader = const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [LumenColors.blue, LumenColors.purple],
      ).createShader(Rect.fromCircle(
          center: Offset(size.width / 2, size.height / 2), radius: radius))
      ..style = PaintingStyle.stroke
      ..strokeWidth = (size.width / 100) * 8;

    canvas.drawCircle(
        Offset(size.width / 2, size.height / 2), radius, _breathPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}
