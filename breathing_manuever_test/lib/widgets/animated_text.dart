import 'package:breathing_manuever_test/styles/text_styles.dart';
import 'package:flutter/material.dart';

class AnimatedText extends StatefulWidget {

  ///The data to show
  final String text;
  final Duration duration;

  const AnimatedText({
    Key? key,
    required this.text,
    this.duration = const Duration(milliseconds: 400),
  }) : super(key: key);

  @override
  _AnimatedTextState createState() => _AnimatedTextState();
}

class _AnimatedTextState extends State<AnimatedText>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;
  late String textToAnimate;

  @override
  void initState() {
    super.initState();
    textToAnimate = widget.text;
    controller = AnimationController(vsync: this, duration: widget.duration)
      ..addListener(() => setState(() {}))
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          textToAnimate = widget.text;
          controller.reverse(from: 1.0);
        } 
      });

    animation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Curves.easeIn,
        reverseCurve: Curves.easeOut,
      ),
    );
    if (widget.text != textToAnimate) {
      controller.forward(from: 0.0);
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(AnimatedText oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.text != oldWidget.text) {
      textToAnimate = oldWidget.text;
      controller.forward(from: 0.0);
    }
  }

  @override
  Widget build(BuildContext context) => Opacity(
        opacity: 1.0 - animation.value,
        child: Text(textToAnimate, style: TextStyles.blueBoldText,),
      );
}
