import 'package:breathing_manuever_test/styles/lumen_colors.dart';
import 'package:dashed_circle/dashed_circle.dart';
import 'package:flutter/material.dart';

class DashedCircles extends StatelessWidget {
  const DashedCircles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: _buildCircles(howMany: 6, padding: 25),
    );
  }

  Widget _buildCircles({required int howMany,required double padding}) {
    double gapSize = 1;
    late Widget result;
    for (int i = 0; i <= howMany; i++) {
      if (i == 0) {
        result = DashedCircle(
          dashes: i * 14 + 40,
          color: LumenColors.lightGrey,
          gapSize: gapSize,
        );
      } else {
        result = DashedCircle(
          dashes: i * 14 + 40,
          gapSize: gapSize,
          color: LumenColors.lightGrey,
          child: Padding(
            padding: EdgeInsets.all(padding),
            child: result,
          ),
        );
      }
    }
    return result;
  }
}
