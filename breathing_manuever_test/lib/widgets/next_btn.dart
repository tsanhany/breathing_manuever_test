import 'package:breathing_manuever_test/styles/lumen_colors.dart';
import 'package:flutter/material.dart';

class NextBtn extends StatelessWidget {
  final Function()? onPressed;
  final String text;
  const NextBtn({required this.onPressed, required this.text, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
                  style: ButtonStyle(
                    splashFactory: NoSplash.splashFactory,
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: const BorderSide(color: LumenColors.lightGrey)))),
                  onPressed: onPressed,
                  child: Text(text));
  }
}
