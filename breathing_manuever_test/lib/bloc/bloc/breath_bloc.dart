import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:breathing_manuever_test/data/breath_flow.dart';
import 'package:breathing_manuever_test/data/models/breath_phase.dart';
import 'package:breathing_manuever_test/utils/constants.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'breath_event.dart';
part 'breath_state.dart';

class BreathBloc extends Bloc<BreathEvent, BreathState> {
  StreamSubscription<int>? breathSubject;

  BreathBloc() : super(BreathInitial()) {
    on<StartInhalingEvent>((event, emit) {
      _startInhaling(emit);
    });
    on<BreathValueReceivedEvent>((event, emit) 
      => _mapBreathValue(event.percentage, emit));
  }

  _startInhaling(Emitter emit) {
    breathSubject?.cancel;
    breathSubject = BreathFlow().breathStream().listen((perc) {
      add(BreathValueReceivedEvent(perc));
    });
  }

  _mapBreathValue(int perc, Emitter emit) {
    BreathPhase phase;
    if (perc < 10) {
      phase = BreathPhase(
          id: Constants.startPhaseId,
          text: Constants.startPhaseText,
          percentage: perc);
    } else if (perc < 90) {
      phase = BreathPhase(
          id: Constants.secondPhaseId,
          text: Constants.secondPhaseText,
          percentage: perc);
    } else if (perc < 100){
      phase = BreathPhase(
          id: Constants.thirdPhaseId,
          text: Constants.finalPhaseText,
          percentage: perc);
    } else {
      phase = BreathPhase(
          id: Constants.finalPhaseId,
          text: Constants.finalPhaseText,
          percentage: perc);
    }
    emit(BreathInProgress(phase));
  }
}
