part of 'breath_bloc.dart';

@immutable
abstract class BreathEvent {}

class StartInhalingEvent extends BreathEvent {}

class BreathValueReceivedEvent extends BreathEvent {
  final int percentage;
  BreathValueReceivedEvent(this.percentage);
}
