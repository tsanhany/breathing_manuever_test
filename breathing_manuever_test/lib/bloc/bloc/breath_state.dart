part of 'breath_bloc.dart';

@immutable
abstract class BreathState extends Equatable{
  final BreathPhase phase;
  const BreathState({required this.phase});
}

class BreathInitial extends BreathState {
   BreathInitial() : super(phase: 
    BreathPhase(
      id: Constants.initialPhaseId, 
      text: '', 
      percentage: 0));

  @override
  List<Object?> get props => [super.phase];
}

class BreathInProgress extends BreathState {
  const BreathInProgress(BreathPhase phase) : super(phase: phase);

  @override
  List<Object?> get props => [phase.id, phase.text, phase.percentage];
  
}

