import 'package:equatable/equatable.dart';

class BreathPhase {
  final int id;
  final String text;
  final int percentage;

  BreathPhase({required this.id, required this.text, required this.percentage});
}

