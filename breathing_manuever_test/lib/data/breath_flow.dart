class BreathFlow {
  Stream<int> breathStream() {
 final flow = [1,4,10,15,20,22,28,40,43,45,57,60,63,69,82,88,90,92,95,100];
 return Stream.periodic(const Duration(milliseconds: 400), (i) => flow[i])
     .take(flow.length);
}



}