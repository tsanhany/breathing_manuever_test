import 'package:breathing_manuever_test/bloc/bloc/breath_bloc.dart';
import 'package:breathing_manuever_test/pages/breath_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    return MaterialApp(
      title: 'Breathing Manuever',
      theme: ThemeData(
        fontFamily: 'Urbanist',
      ),
      home: BlocProvider(
        create: (context) => BreathBloc(),
        child: const BreathScreen(),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

