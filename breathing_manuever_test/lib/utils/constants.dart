class Constants {
  // Phases Id's
  static const int initialPhaseId = 0;
  static const int startPhaseId = 1;
  static const int secondPhaseId = 2;
  static const int thirdPhaseId = 3;
  static const int finalPhaseId = 4;
  // Phases Texts
  static const String startPhaseText = 'Start Inhaling';
  static const String secondPhaseText = 'Keep Going!!!';
  static const String finalPhaseText = 'Well Done!';
  // Phases Button texts
  static const String phase1Btn_start = "start";
  static const String phase4Btn_replay = "replay";
}
